<?php

namespace Drupal\generate_user_by_mail\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Configure Generate user by mail settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'generate_user_by_mail_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['generate_user_by_mail.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    $node_field = [];
    foreach ($types as $key => $type) {
      $definitions = \Drupal::service('entity_field.manager')
        ->getFieldDefinitions('node', $key);
      foreach ($definitions as $field_name => $definition) {
        if ($definition->getType() == 'email') {
          $node_field[$key . '#' . $field_name] = $type->get('name') . '/' . $definition->getLabel();
        }
      }
    }
    $form['content_usermail_field'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content/email field'),
      '#description' => $this->t('Select content and email field to auto-create user.'),
      '#options' => $node_field,
    ];
    $default = $this->config('generate_user_by_mail.settings')
      ->get('content_usermail_field');
    if (!empty($default)) {
      $form['content_usermail_field']['#default_value'] = $default;
    }

    $form['content_username_field'] = [
      '#type' => 'select',
      '#title' => $this->t('User name field'),
      '#description' => $this->t('Select how to manage user name.'),
      '#options' => [
        'title' => t("Title"),
        'email' => t("Email field"),
        'hash' => t("Random"),
      ],
    ];
    $default = $this->config('generate_user_by_mail.settings')
      ->get('content_username_field');
    if (!empty($default)) {
      $form['content_username_field']['#default_value'] = $default;
    }

    $roles = Role::loadMultiple();
    unset($roles[Role::ANONYMOUS_ID]);
    unset($roles[Role::AUTHENTICATED_ID]);
    $default_roles = [];
    foreach ($roles as $key => $role) {
      if (!$role->isAdmin()) {
        $default_roles[$key] = $role->label();
      }
    }
    $form['default_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Default roles'),
      '#description' => $this->t('Select roles for the auto-create user.'),
      '#options' => $default_roles,
    ];
    $default = $this->config('generate_user_by_mail.settings')
      ->get('default_roles');
    if (!empty($default)) {
      $form['default_roles']['#default_value'] = $default;
    }

    $form['default_status'] = [
      '#type' => 'radios',
      '#title' => $this->t('User status'),
      '#description' => $this->t('Set user active status at creation.'),
      '#options' => [
        0 => t('Locked'),
        1 => t('Active')
      ],
      '#default_value' => $this->config('generate_user_by_mail.settings')
        ->get('default_status')
    ];

    $form['assign_content'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Assign content to user'),
      '#description' => $this->t('If checked the content which generate user is set as if created from the user itself.'),
      '#default_value' => $this->config('generate_user_by_mail.settings')
        ->get('assign_content')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (is_null($form_state->getValue('default_status'))) {
      $form_state->setErrorByName('default_status', $this->t('Please select a value.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('generate_user_by_mail.settings')
      ->set('content_usermail_field', $form_state->getValue('content_usermail_field'))
      ->set('content_username_field', $form_state->getValue('content_username_field'))
      ->set('default_roles', $form_state->getValue('default_roles'))
      ->set('default_status', $form_state->getValue('default_status'))
      ->set('assign_content', $form_state->getValue('assign_content'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
