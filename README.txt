Generate user by mail

This mudule makes easy and configurable a very specific need: programamtically create a user when a node is created.

You just need a content type with an email field.

In settings you can select which content/field will be catched, how to manage user name (content title, same as email, random hash), default roles and status.

You can also choose to assign the user as content's author.

If the email is already used, no user will be created. If the assign option is checked, the existing user is set as author.